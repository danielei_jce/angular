import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class PostsService {
  postsobservable;
//private _url='https://jsonplaceholder.typicode.com/posts';

getPosts() {
    this.postsobservable = this.af.database.list('/posts').map(
      posts=>{
         posts.map(
          post=> {
            post.username = [];
            for(var u in post.users){
              post.username.push(
                this.af.database.object('/users/'+u)
              )
            }
          }
        );
        return posts; 
      } 
    )
    ;
    return this.postsobservable;
    //this.postsobservable = this.af.database.list('/posts');
    //return this.postsobservable;
   }
  addPost(post){
    this.postsobservable.push(post);
  }
  updatePost(post){
    let postKey = post.$key;
    let postData = {title:post.title, body:post.body};
    this.af.database.object('/posts/'+postKey).update(postData);
  }
  deletePost(post){
    let postKey = post.$key;
    this.af.database.object('/posts/'+ postKey).remove();
  }
  constructor(private af:AngularFire) { }

}
