import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  IsLoading:Boolean = true;
  posts;

  constructor(private _postsService:PostsService){ }
  deletePost(post){
    //this.posts.splice(this.posts.indexOf(post),1)  }
    this._postsService.deletePost(post);
  }
  savePost(post){
    this.posts[this.posts.indexOf(post)].title = post.title;
    this.posts[this.posts.indexOf(post)].body = post.body;
  }
  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData=> 
    {this.posts=postsData
      this.IsLoading = false});
    console.log(this.posts);
  }
  addPost(post){
    this._postsService.addPost(post);
  }
  updatePost(post){
    this._postsService.updatePost(post);
  }

}
