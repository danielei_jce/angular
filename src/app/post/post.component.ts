import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  post:Post;
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() saveEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  title1;
  body1;
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.post)
  }
   toggleEdit(){
    this.isEdit? this.sendCurrentDetails() : this.seveOldDetails();
    this.isEdit = !this.isEdit;
    this.isEdit? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
    if(!this.isEdit){ this.editEvent.emit(this.post);}
  }
  seveOldDetails(){
    this.title1 = this.post.title;
    this.body1 = this.post.body;
  }
  sendCurrentDetails(){
    this.saveEvent.emit(this.post)
  }
  sendCancle(){
    this.isEdit? this.cenceldDetails() : this.toggleEdit();
  }
  cenceldDetails(){
    this.post.title = this.title1;
    this.post.body = this.body1;
   // this.editButtonText = 'Save';
  }
  ngOnInit() {
  }

}
